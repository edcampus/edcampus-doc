.. EDCampus documentation master file, created by
   sphinx-quickstart on Thu Jul 25 11:32:17 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EDCampus's documentation!
====================================

.. toctree::
    :maxdepth: 2
    :glob:
    
    presentation
    concepts
    gettingStartedAsDevelopper
    forDeveloperGlobalArchitecture
    forDeveloperFront
    forDeveloperBack
    forDeveloperFuture 
    forAdministrator
    community