Communauté
==========

Contact
-------

- Anthony Geourjon, anthony.geourjon@univ-grenoble-alpes.fr
- Gérard Pollier, gerard.pollier@univ-grenoble-alpes.fr

Partenaires
-----------

- Disrupt Campus Grenoble
- COMUE Grenoble
- UHA 4.0
- Polytech Grenoble


Communauté d'utilisateurs
-------------------------

Disrupt Campus Grenoble
IUT1 de Grenoble
Formation Green University